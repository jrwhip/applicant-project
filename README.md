# Applicant Project Jared Whipple

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.1.

## Setup App

1. Go to [firebase](https://firebase.google.com) and create a project.
2. Create a Firestore database  
    * Select Database from Develop menu in sidebar
    * Click 'Create database' button
    * Choose start in test mode
    * Choose a location for you database (nam5 US Centeral)
3. Create Storage
    * Select Storage from Develop menu in sidebar
    * Click 'Get started' button and follow steps
    * From Storage select Rules tab and change permisions to allow non auth to upload  
    ```javascript
    service firebase.storage {
      match /b/{bucket}/o {
        match /{allPaths=**} {
          allow read, write;
        }
      }
    }
    ```
    * Select publish
    * Copy storage folder path and paste into environment.ts file.
4. Add key to project
    * Select 'Project Settings' from 'Project Overview' settings gear in sidebar
    * copy Web API Key and Project Id and paste into environment.ts file

## Development server

Run `ng serve`, `npm run start`, or `yarn start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build`, `npm run build`, or `yarn build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

note creating a build is not nesisary to test this app.

## Running unit tests

Run `ng test`, `npm run test`, or `yarn test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help`, `npm run help`, or `yarn help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
