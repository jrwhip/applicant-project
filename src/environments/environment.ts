// Intstructions: Update with values from https://firebase.google.com
export const environment = {
  production: false,
  firebase: {
    apiKey: '<API KEY>',
    /* FOLDER PATH NAME ONLY i.e. for gs://goreact-1234abc.appspot.com/ USE goreact-1234abc.appspot.com */
    storageBucket: '<FOLDER PATH NAME>',
    projectId: '<PROJECT ID>',
  }
};
