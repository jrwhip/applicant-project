import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { map } from 'rxjs/operators';

export interface UploadedFiles { downloadURL: string; path: string; name: string; }

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ListViewComponent implements OnInit {
  files$: any;

  constructor(private db: AngularFirestore) { }

  ngOnInit(): void {
    this.getList();
  }

  getList() {
    this.files$ = this.db.collection('files').snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as UploadedFiles;
        data.downloadURL = encodeURIComponent(data.downloadURL);
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );

  }

}
