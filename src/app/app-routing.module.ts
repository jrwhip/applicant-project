import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploaderComponent } from './uploader/uploader.component';
import { ListViewComponent } from './list-view/list-view.component';
import { DetailedViewComponent } from './detailed-view/detailed-view.component';

const routes: Routes = [
  { path: 'upload', component: UploaderComponent },
  { path: 'list', component: ListViewComponent },
  { path: 'detail/:url', component: DetailedViewComponent },
  { path: '', redirectTo: '/upload', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
