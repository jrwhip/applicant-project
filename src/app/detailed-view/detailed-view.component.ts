import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-detailed-view',
  templateUrl: './detailed-view.component.html',
  styleUrls: ['./detailed-view.component.scss']
})
export class DetailedViewComponent implements OnInit {
  params$: Observable<string>;
  isMovie = false;

  constructor(private activatedRoute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.params$ = this.activatedRoute.params.pipe(
      tap(v => this.isMovie = /mp4/.test(v.url.toLowerCase())),
      map(v => decodeURIComponent(v.url))
     );
  }

}
