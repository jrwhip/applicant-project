import { Component } from '@angular/core';

@Component({
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss']
})
export class UploaderComponent {
  files: File[] = [];
  msg = 'Select file or files to upload';

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      if (files[i].type === 'image/jpeg' || files[i].type === 'video/mp4') {
        this.files.push(files.item(i));
        this.msg = 'Select file or files to upload';
      } else {
        this.msg = 'Files must be either JPG or MP4';
      }
    }
  }
}
