import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { FileDropDirective } from './shared/file-drop.directive';
import { UploaderComponent } from './uploader/uploader.component';
import { UploadProgressComponent } from './shared/upload-progress/upload-progress.component';
import { ListViewComponent } from './list-view/list-view.component';
import { DetailedViewComponent } from './detailed-view/detailed-view.component';
import { HeaderComponent } from './shared/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    FileDropDirective,
    UploaderComponent,
    UploadProgressComponent,
    ListViewComponent,
    DetailedViewComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
